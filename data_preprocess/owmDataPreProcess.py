# -*- coding: utf-8 -*-
"""
Created on Sat Aug 31 21:52:30 2019

@author: magya
"""
import json
import pandas as pd
from datetime import datetime


## ---------------------------------------------------------------------- ##
def extendMeasurementsOnWholeRange(dataFrame, frequency, columnName):
    """
    Extends the dates by flooring down the whole date range to given frequency (from /hour to /minutes)
    
    First it creates a date range object by given frequency (ex. minutes), then it merges with the original data frame
    Sort the values by natural order, then resets the indexes to be more visible what id does.
    
    """
    date_rng = pd.date_range(dataFrame[columnName][0], dataFrame[columnName][len(dataFrame)-1], freq=frequency)
    temporaryDataFrame= pd.DataFrame({columnName : pd.to_datetime(pd.Series(date_rng))})
    dataFrame = pd.merge(dataFrame, temporaryDataFrame, on=columnName, how='right')
    
    return dataFrame.sort_values(by=[columnName]).reset_index(drop=True)
    

## ---------------------------------------------------------------------- ##
def isoTimeToDateTime(isoTimeString):
     return datetime.utcfromtimestamp(isoTimeString)


## ---------------------------------------------------------------------- ##
def kelvinToCelsius(kelvin):
    celsius = kelvin - 273.15
    return celsius


## ---------------------------------------------------------------------- ##
def getClimateReadingsFromFile(filename):
    with open(filename) as json_file:
        data = json.load(json_file)

    return data



def getPreProcessedOutsideTemperatureFromOwm():
    """
    Main function for getting all the preprocessed data by this file.
    
    Return  with a dataFrame of measurements/minute for the whole available range
    """
    
    
    owm_readings = getClimateReadingsFromFile('owm_data_2019_02_01-2019-03-31.json')
    
    ## extract the valuable data from the raw data
    outsideTemperatures = [kelvinToCelsius(i['main']['temp']) for i in owm_readings]
    isoDate = [isoTimeToDateTime(i['dt']) for i in owm_readings]
    
    ## create the dataFrame with the measurements
    owm_df = pd.DataFrame({'outsideTemperatures': outsideTemperatures, 'isoDate': isoDate})
    
    owm_df = extendMeasurementsOnWholeRange(owm_df, '1T', 'isoDate')
    
        ## correct the current tempereture by filling the data between two known temperature
    owm_df['correctedOutsideTemp'] = ((owm_df['outsideTemperatures'].fillna(method='ffill') + owm_df['outsideTemperatures'].fillna(method='bfill'))/2)
    
    return owm_df
    

def printOwmDataFrame(dataFrame) :
    dataFrame.set_index('isoDate').plot()
