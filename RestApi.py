# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 14:26:52 2019

@author: magya
"""

import json
from datetime import datetime
from datetime import timedelta
from urllib.parse import quote

import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from dateutil.tz import tzoffset
from requests import get
from matplotlib import pyplot as plt

import data_preprocess.owmDataPreProcess as owmData

endDateTime = datetime.now(tzoffset('UTC', 2*60*60)).replace(microsecond=0)
endDateTime = endDateTime
startDateTime = endDateTime - timedelta(days=1)

entities = quote('climate.study')


## ---------------------------------------------------------------------- ##
def extendMeasurementsOnWholeRange(dataFrame, frequency, columnName):
    """
    Extends the dates by flooring down the whole date range to given frequency (from /hour to /minutes)
    
    First it creates a date range object by given frequency (ex. minutes), then it merges with the original data frame
    Sort the values by natural order, then resets the indexes to be more visible what id does.
    
    """
    date_rng = pd.date_range(dataFrame[columnName][0], dataFrame[columnName][len(dataFrame)-1], freq=frequency)
    temporaryDataFrame= pd.DataFrame({columnName : pd.to_datetime(pd.Series(date_rng))})
    dataFrame = pd.merge(dataFrame, temporaryDataFrame, on=columnName, how='right')
    
    return dataFrame.sort_values(by=[columnName]).reset_index(drop=True)

## ---------------------------- ##
def saveClimateReading (startDate, endDate, climateReadings):
    with open('data_'+ startDate.strftime("%Y-%m-%dT-%H-%M-%S") + '_' + endDate.strftime("%Y-%m-%dT-%H-%M-%S") +'.json', 'w') as outfile:
        json.dump(climateReadings, outfile)
## ---------------------------- ##
        
def isBlank (myString):
    """
    Decides if a string is blank or not
    
    Returns a boolean value, true if the given string is empty
    
    Parameter
    ---------
    
    mystring : string
        Given string
    """
    return not (myString and myString.strip())
        
## ---------------------------- ##
def getClimateReadings(startDate, endDate, entities):
    """
    Reads the entity values from historical data between startDate and endDate
    
    Returns a list with the entities and values in json format
    
    Parameters
    ----------
    startDate : string
        Start date of the requested historical values
    endDate : string
        End date of the requested historical values
    entities : string 
        List of the requested entities 
    
    """
    ## format should be YYYY-MM-DDThh:mm:ssTZD
    startDateIsoFormat = startDate.isoformat()
    endDateIsoFormat = '?end_time=' + quote(endDate.isoformat())
    
    entityFilter =  '&filter_entity_id=' if not isBlank(endDateIsoFormat) else '?filter_entity_id='
    
    url = 'https://ermihome.go.ro/api/history/period/' + startDateIsoFormat + endDateIsoFormat + entityFilter + entities
    print(url)
    headers = {
        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI4MWIzODhlYzNkYzI0MmY1OThkNjM4MDI2OTEyZTJiYiIsImlhdCI6MTU2NTQ0NTMxMiwiZXhwIjoxODgwODA1MzEyfQ.FlU9VfAgys6eefB9L_Uz03fwe7pq2ui90ETKzEQI08U',
        'content-type': 'application/json',
    }
    
    response = get(url, headers=headers)
    #   print(response.text)
    
    climateArray = json.loads(response.text)
    
    ##saveClimateReading(startDate, endDate, climateArray)
    
    return climateArray

## ---------------------------------------------------------------------- ##

def getClimateReadingsFromFile(filename):
    with open(filename) as json_file:
        data = json.load(json_file)

    return data

## ---------------------------------------------------------------------- ##
def eliminateNoise(values, std_factor = 2):
    mean = np.nanmean(values)
    standard_deviation = np.nanstd(values)

    if standard_deviation == 0:
        return values

    final_values = [element for element in values if element > mean - std_factor * standard_deviation]
    final_values = [element for element in final_values if element < mean + std_factor * standard_deviation]

    return final_values
## ---------------------------------------------------------------------- ##
    

def convertStateValuesToBinary (stateValues):
    """
     convert heating system state values to binary
     heat means that the system was turned on -> 1
         returns an array of 0 and 1-s
    """
    statesList = [];
    for i, v in stateValues.items():
        if v == 'heat':
            statesList.append(1)
        else:
            statesList.append(0)
            
    return statesList


def correctDataWithLinearRegression() :
    
    X_data_set = df['lastChangedDate'] #independent variable - lastchangedDate
    X_data_set = X_data_set.values.reshape(-1,1)
    y_data_set = df.iloc[:, 4].values #dependent variable - correctedTemp
    
    """
        DATA ERROR CORRECTION: change the peaks of the data errors to correct values
        where the standard deviation is too big
    
    """
    # Splitting the dataset into the Training set and Test set
    # --------------------------------------------------------
    from sklearn.cross_validation import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X_data_set, y_data_set, test_size=1/3, random_state=0)
    
    
    # Fitting Simple Linear Regression to the Training set
    from sklearn.linear_model import LinearRegression
    regressor = LinearRegression()
    regressor.fit(X_train, y_train)
    
    # Visualising the Training set results
#    plt.scatter(X_train, y_train, color='red')
#    plt.plot(X_train, regressor.predict(X_train), color='blue')
#    plt.title('Time vs temperature (Training set)')
#    plt.xlabel('Time')
#    plt.ylabel('Temperature')

    # Predicting the Test set results
    y_pred = regressor.predict(X_test)
    
    # Visualising the Test set results
#    plt.scatter(X_test, y_test, color='red')
#    plt.plot(X_train, regressor.predict(X_train), color='blue')
#    plt.title('Salary vs Experience (Training set)')
#    plt.xlabel('Years of Experience')
#    plt.ylabel('Salary')
#    plt.show()
    


# climateArray = getClimateReadings(startDateTime, endDateTime, entities)

climateArray = getClimateReadingsFromFile('data_2019-02-28T-23-20-54_2019-03-15T-23-20-54.json')


## extract the valuable data from the raw data
currentTemperatures = [i['attributes']['current_temperature'] for i in climateArray[0]]
lastChangedDate = [i['last_updated'] for i in climateArray[0]]
state = [i['state'] for i in climateArray[0]]
setTemperature = [i['attributes']['temperature'] for i in climateArray[0]]

## create the dataFrame with the measurements
df = pd.DataFrame({'currentTemperatures':currentTemperatures, 'lastChangedDate':lastChangedDate, 'state':state, 'setTemperature':setTemperature})

df['lastChangedDate']  = pd.to_datetime(df['lastChangedDate'])

#climateArray['lastChangedDate']  = pd.to_datetime(climateArray['lastChangedDate'])

filtered_temperature = [] # here we keep the temperature values after removing outliers
##filtered_temperature.append(np.mean(eliminateNoise(i for i in df['currentTemperatures'].values.tolist())))


df['state'] = pd.Series(convertStateValuesToBinary(df['state']))      

df['lastChangedDate'] = pd.to_datetime(df['lastChangedDate']).dt.floor('1T')



"""
# ----------------------------------------------------------------------------
## DATA PRE-PROCESSING: extending data to have measurements for every minute
    ## extending the dataset to have a measurement for every minute. 
    ## Side effect, there is no data for every minute, since the extending
    ## does not provide measurements, just date
"""    
df = extendMeasurementsOnWholeRange(df, '1T', 'lastChangedDate')



"""
# ----------------------------------------------------------------------------
    ## DATA PROCESSING: Fix the side-effect by filling the missing data
        ## set the last changed date as the index of the dataset
"""
## correct the current tempereture by filling the data between two known temperature
df['correctedTemp'] = ((df['currentTemperatures'].fillna(method='ffill') + df['currentTemperatures'].fillna(method='bfill'))/2)

## correct the state between two known state, assuming that between two 0 the state didn't changed
df['correctedState'] = ((df['state'].fillna(method='bfill')))

## correct the set temperature by filling between two known temperature
df['correctedSetTemp'] = ((df['setTemperature'].fillna(method='bfill')))



"""
# ----------------------------------------------------------------------------
    ## DATA ANALYSIS: it is possible that the measurements are not correct.
    ## to find out where, let's calculate the standard deviation, to see where is that
"""
min = df['correctedTemp'].min()
mean = df['correctedTemp'].mean()
std = df['correctedTemp'].std()
max = df['correctedTemp'].max()

df2 = df.set_index('lastChangedDate')

#resample /hour and aggregate mean
resampledMean= df2['correctedTemp'].resample('H').mean()
#resample /hour and aggregate standard deviation to find out where could be possible measurement errors
resampledStd = df2['correctedTemp'].resample('H').std()


"""
## DATA PROCESSING
# ----------------------------------------------------------------------------
TODO:DEAL WITH THE HIGH STANDARD DEVIATION ZONES LATER!!!
##csoportosítani óránként majd erre az órára átlagot számolni a hőmérsékletből,
# és ott ahol kiesik a szórásból (peak értékek) kicserélni erre az óránkénti átlagra
"""


"""
## CREATE BLOCKS FOR SCENARIOS

10 minutes intervals which will be 1/6 part of a scenario
# ----------------------------------------------------------------------------
"""


heatingMinutesPerInterval = df2['correctedState'].resample('10T').sum()
averageTemperaturePerInterval = df2['correctedTemp'].resample('10T').mean()
setTemperaturePerInterval = df2['correctedSetTemp'].resample('10T').mean()

scenarioBlockDataFrame = pd.DataFrame({'heatingMinutes': heatingMinutesPerInterval,
                                   'averageTemp': averageTemperaturePerInterval,
                                   'setTemp': setTemperaturePerInterval})
        
#scenarioBlockDataFrame.plot()


"""
## 3r Party temperature request for outsideTemp
# ----------------------------------------------------------------------------
"""

outsideTemperatureDataFrame = owmData.getPreProcessedOutsideTemperatureFromOwm()

"""
## Integrate outside temp with the used datastructure
# ----------------------------------------------------------------------------
"""

outsideTemperatureDataFrame = outsideTemperatureDataFrame.set_index('isoDate').tz_localize(tz='UTC')

outsideMeanTemperaturePerInterval = outsideTemperatureDataFrame['correctedOutsideTemp'].resample('10T').mean()
outsideMeanTempForScenarios = pd.DataFrame({'outsideTemp': outsideMeanTemperaturePerInterval})

scenarioBlockDataFrame = pd.merge(scenarioBlockDataFrame, outsideMeanTempForScenarios, left_index=True, right_index=True)


"""
## Feature Engineering: creating features which will increase to the accuracy of the model.
## Calculating of outsidetempdif, tempdif, ....
# ----------------------------------------------------------------------------
#tempDif = currTemp - setTemp
#outTempDif = outsideTemp - setTemp
"""

scenarioBlockDataFrame['tempDif'] = scenarioBlockDataFrame['averageTemp'] - scenarioBlockDataFrame['setTemp']
scenarioBlockDataFrame['outTempDif'] = scenarioBlockDataFrame['outsideTemp'] - scenarioBlockDataFrame['setTemp']



"""
## CREATE SCENARIOS

Flat datastructure which contains:
6 blocks of 10 minutes interval,
average outside temp,
average inside temp,
average of the tempdif, outsidetempdif
# ----------------------------------------------------------------------------
"""

avgScenarioTemperature = scenarioBlockDataFrame.resample('H')['averageTemp'].mean()
avgScenarioSetTemp = scenarioBlockDataFrame.resample('H')['setTemp'].mean()
avgScenarioOutsideTemp = scenarioBlockDataFrame.resample('H')['outsideTemp'].mean()
avgScenarioTempDif = scenarioBlockDataFrame.resample('H')['tempDif'].mean()
avgScenarioOutTempDif = scenarioBlockDataFrame.resample('H')['outTempDif'].mean()
sumScenarioHeatingMinutes = scenarioBlockDataFrame.resample('H')['heatingMinutes'].sum()
countScenarioTurnedOn = scenarioBlockDataFrame.resample('H')['heatingMinutes'].apply(lambda x: (x > 0).sum())

scenarioDataFrame = pd.DataFrame({'avgScenarioTemperature': avgScenarioTemperature,
                                   'avgScenarioSetTemp': avgScenarioSetTemp,
                                   'avgScenarioOutsideTemp': avgScenarioOutsideTemp,
                                   'avgScenarioTempDif': avgScenarioTempDif,
                                   'sumScenarioHeatingMinutes': sumScenarioHeatingMinutes,
                                   'avgScenarioOutTempDif': avgScenarioOutTempDif,
                                   'countScenarioTurnedOn': countScenarioTurnedOn})
        
#scenarioDataFrame.plot()



"""
## SCORE CALCULATION
# ----------------------------------------------------------------------------

+ lowest turned on minutes / hour (for for efficiency)
+ tempdif low (for comfort)
+ tempdif converge to to setTemp lowest time possible
- overshooting (use exponential function to multiply a value - bigger the overshoot, bigger the penalty)

TODO: REFINE scoring by some sophisticated algorithm
"""
scenarioScore = scenarioDataFrame['countScenarioTurnedOn'].apply(lambda x: x * 10 * 60) +\
scenarioDataFrame['sumScenarioHeatingMinutes'].apply(lambda x: x * 2) - \
scenarioDataFrame['avgScenarioTempDif'].apply(lambda x: 3600 if x > 0 else 0)

scenarioScoreDataFrame = pd.DataFrame({'scenarioScore': scenarioScore})

"""
## SCENARIO ASSEMBLING
"""
scenarioDataFrame = pd.merge(scenarioDataFrame, scenarioScoreDataFrame, left_index=True, right_index=True)

scenarioFullDataFrame = pd.concat([scenarioDataFrame, scenarioBlockDataFrame], axis=1)



"""
## CREATE SCENARIO GROUPS
# ----------------------------------------------------------------------------
"""
scenarioFullDataFrame.plot(x='avgScenarioTempDif', y='avgScenarioOutTempDif', style='o')

"""
## DEFINE CLUSTER NUMBERS WITH ELBOW METHOD

## Where the line breaks for the first time that is the number for the optimal cluster size

"""

numpyArray = scenarioDataFrame[['avgScenarioTempDif', 'avgScenarioOutTempDif']].to_numpy()

wcss = []
for i in range(1, 11):
    kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=300, n_init=10, random_state=0)
    kmeans.fit(numpyArray)
    wcss.append(kmeans.inertia_)
  
plt.figure()     
plt.plot(range(1, 11), wcss)
plt.title('Elbow Method')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS')
plt.show()



"""
## GROUPS CLUSTERING
# ----------------------------------------------------------------------------

## Cluster number from the previous method
"""
#plt.plot(climateArray['attributes']['current_temperature'], climateArra['attributes']['last_updated']);
scenarioBlockDataFrame.plot()

kmeans = KMeans(n_clusters=2, init='k-means++', max_iter=300, n_init=10, random_state=0)
pred_y = kmeans.fit_predict(numpyArray)
plt.figure()  
plt.scatter(numpyArray[:,0], numpyArray[:,1])
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=300, c='red')
plt.show()



