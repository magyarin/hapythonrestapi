from setuptools import setup

setup(
    name='rest_api',
    version='',
    packages=['data_preprocess'],
    url='',
    license='',
    author='magya',
    author_email='',
    description='', install_requires=['numpy', 'pandas', 'dateutil', 'requests', 'scikit-learn', 'matplotlib']
)
