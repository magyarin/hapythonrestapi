from flask import Flask, jsonify

app = Flask(__name__)

scenarios = [
    {
        'id': 1,
        'viEpoch': 'epoch time'
        , 'vGroup': 'group'
        , 'vTempDif': 0.3
        , 'vOutTempDif': 1
        , 'run0': 0
        , 'run1': 2
        , 'run2': 0
        , 'run3': 2
        , 'run4': 0
        , 'run5': 2
    },
    {
        'id': 2,
        'viEpoch': 'epoch time'
        , 'vGroup': 'group'
        , 'vTempDif': 0.3
        , 'vOutTempDif': 1
        , 'run0': 0
        , 'run1': 2
        , 'run2': 0
        , 'run3': 2
        , 'run4': 0
        , 'run5': 2
    },
]

@app.route('/scenario/get_scenario', methods=['GET'])
def get_tasks():
    return jsonify({'scenarios': scenarios})

@app.route('/initialize', methods=['GET'])
def initialize_success_response():
    return Flask.Response(status = 200)

@app.route('/', methods=['GET'])
def invalid_request():
    return Flask.Response(status = 400)


if __name__ == '__main__':
    app.run()
